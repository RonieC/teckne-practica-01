'use strict';

angular.module('proyectoFullstackApp')
  .controller('JavaCtrl', function ($scope, $http, $state) {
    var vm = this;
    vm.fillList = fillList;

    $scope.$on('details-updated', function (event, params) {
      fillList();
    });

    activate();

    function activate() {
      fillList();
    }

    function fillList() {
      $http.get('/api/cursos/1').success(function (response) {
        vm.inscritos = response.inscritos;
      });
    }

    vm.goDetails = function (id) {
      $state.go('main.java.detail',
        {
          id: id
        }
      );
    };
  });
