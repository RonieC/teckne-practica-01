'use strict';

describe('Controller: ACtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var ACtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ACtrl = $controller('ACtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
