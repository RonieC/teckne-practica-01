'use strict';

angular.module('proyectoFullstackApp')
    .service('Estudiante', function ($q, $resource) {
        var resource = $resource('/api/estudiantes/:id', {id: '@id'}, {
            // Not needed
            get: {
                method: 'GET'
            },
            // Custom
            saveChanges: {
                method: 'PUT'
            }
        });

        var get = function (id) {
            var estudiante = $q.defer();

            resource.get({id: id}).$promise.then(function (response) {
                estudiante.resolve(response);
            }, function (error) {
                estudiante.reject(error);
            });

            return estudiante.promise;
        };

        var put = function(id, payload) {
            var estudiante = $q.defer();

            resource.saveChanges({id: id}, payload).$promise.then(function (response) {
                estudiante.resolve(response);
            }, function (error) {
                estudiante.reject(error);
            });

            return estudiante.promise;
        }

        return {
            get: get,
            save: put
        };
    });
